import java.util.Scanner;

public class Company {
    public static void main(String[] args){
        Employee e1 = new Employee();
        Employee e2 = new Employee();
        System.out.println("Enter Employee 1 Details: Name, ID, Salary");
        Scanner in = new Scanner(System.in);
        e1.setEmployeeName(in.next());
        e1.setEmpId(in.nextInt());
        e1.setSalary(in.nextInt());
        System.out.println("Enter Employee 2 details: Name, Id, Salary");
        e2.setEmployeeName(in.next());
        e2.setEmpId(in.nextInt());
        e2.setSalary(in.nextInt());

        if(e1.getSalary() == e2.getSalary())
        {
            System.out.println("True");
        }
        else
        {
            System.out.println("False");
        }
    }
}
