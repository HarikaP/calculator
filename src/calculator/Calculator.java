package calculator;

public class Calculator implements IntegerCalculator,FloatCalculator,LongCalculator,DoubleCalculator{
    @Override
    public void add(int a, int b) {
        System.out.println(a+b);
    }

    @Override
    public void sub(int a, int b) {
        System.out.println(a-b);
    }

    @Override
    public void multiply(int a, int b) {
        System.out.println(a*b);
    }

    @Override
    public void divide(int a, int b) {
        System.out.println(a/b);
    }
    @Override
    public void add(float a, float b) {
        System.out.println(a+b);
    }

    @Override
    public void sub(float a, float b) {
        System.out.println(a-b);
    }

    @Override
    public void multiply(float a, float b) {
        System.out.println(a*b);
    }

    @Override
    public void divide(float a, float b) {
        System.out.println(a/b);
    }
    @Override
    public void add(long a, long b) {
        System.out.println(a+b);
    }

    @Override
    public void sub(long a, long b) {
        System.out.println(a-b);
    }

    @Override
    public void multiply(long a, long b) {
        System.out.println(a*b);
    }

    @Override
    public void divide(long a, long b) {
        System.out.println(a/b);
    } @Override
    public void add(double a, double b) {
        System.out.println(a+b);
    }

    @Override
    public void sub(double a, double b) {
        System.out.println(a-b);
    }

    @Override
    public void multiply(double a, double b) {
        System.out.println(a*b);
    }

    @Override
    public void divide(double a, double b) {
        System.out.println(a/b);
    }
}
