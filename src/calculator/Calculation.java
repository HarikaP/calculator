package calculator;

import java.util.Scanner;

public class Calculation {
    public static void main(String[] args){
        System.out.println("Enter the type of numbers:");
        Scanner in = new Scanner(System.in);
        String type = in.next();
        System.out.println("Enter the two numbers on which the arithematic operation is to be performed:");
        if(type.equals("int"))
        {
            int a = in.nextInt();
            int b = in.nextInt();
            Calculator cal = new Calculator();
            cal.add(a,b);
            cal.sub(a,b);
            cal.multiply(a,b);
            cal.divide(a,b);
        }
        else if(type.equals("float"))
        {
            float a = in.nextFloat();
            float b = in.nextFloat();
            Calculator cal = new Calculator();
            cal.add(a,b);
            cal.sub(a,b);
            cal.multiply(a,b);
            cal.divide(a,b);

        }
        else if(type.equals("long")){
            long a =in.nextLong();
            long b = in.nextLong();
            Calculator cal = new Calculator();
            cal.add(a,b);
            cal.sub(a,b);
            cal.multiply(a,b);
            cal.divide(a,b);
        }
        else if(type.equals("double"))
        {
            double a = in.nextDouble();
            double b = in.nextDouble();
            Calculator cal = new Calculator();
            cal.add(a,b);
            cal.sub(a,b);
            cal.multiply(a,b);
            cal.divide(a,b);
        }
        else
        {
            System.out.println("Invalid type of number");
        }




    }
}
