package calculator;

public interface FloatCalculator {
    public void add(float a,float b);
    public void sub(float a,float b);
    public void multiply(float a,float b);
    public void divide(float a,float b);
}
