package calculator;

public interface DoubleCalculator {
    public void add(double a,double b);
    public void sub(double a,double b);
    public void multiply(double a,double b);
    public void divide(double a,double b);
}
