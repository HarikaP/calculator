package calculator;

public interface LongCalculator {
    public void add(long a,long b);
    public void sub(long a,long b);
    public void multiply(long a,long b);
    public void divide(long a,long b);
}
