public class Employee {
    private String employeeName;
    private int empId;
    private int salary;

    public int getEmpId() {
        return empId;
    }

    public int getSalary() {
        return salary;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
